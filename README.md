# FirstFirebaseApp


Example : 
1. gradlew appDistributionLogin
	Contoh
	```
	Please open the following address in your browser:
	https://accounts.google.com/o/oauth2/auth?access_type=offline&client_id=563584335869-fgrhgmd47bqnekij5i8b5pr03ho849e6.apps.googleusercontent.com&redirect_uri=http://localhost:51637/Callback&response_type=code
	&scope=https://www.googleapis.com/auth/cloud-platform
	```
2. Copy semua URL dan buka di browser
	Contoh : 
	```
	Refresh token: 1//0gio1uphl8H1WCgYIARAAGBASNwF-L9IrjR-Qp5EXOKtcnb3Ln5PVuj_1xVGIFrBFbvKDHG_ZlERi4Qtxo2JmY5AdKuM6_33dTJY
	Set the refresh token as an FIREBASE_TOKEN environment variable.
	```
3. Set Environment Variable
	Contoh :
	Windows :
	```
	export FIREBASE_TOKEN=1//0gio1uphl8H1WCgYIARAAGBASNwF-L9IrjR-Qp5EXOKtcnb3Ln5PVuj_1xVGIFrBFbvKDHG_ZlERi4Qtxo2JmY5AdKuM6_33dTJY
	```
	```
	set FIREBASE_TOKEN=1//0gio1uphl8H1WCgYIARAAGBASNwF-L9IrjR-Qp5EXOKtcnb3Ln5PVuj_1xVGIFrBFbvKDHG_ZlERi4Qtxo2JmY5AdKuM6_33dTJY
	```
4. gradlew appDistributionUploadRelease 
4. (optional) gradlew assembleRelease appDistributionUploadRelease